package ru.shariktlt.templates.compiler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import ru.shariktlt.templates.compiler.dto.CompilerConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigurationReader {

    public static final String CONFIG_TEMPLATE_YAML = "./configTemplate.yaml";
    private final  ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    public CompilerConfiguration readConfiguration(String configFile) throws IOException {
        if(configFile == null){
            configFile = CONFIG_TEMPLATE_YAML;
        }
        return mapper.readValue(new File(configFile), CompilerConfiguration.class);
    }
}
